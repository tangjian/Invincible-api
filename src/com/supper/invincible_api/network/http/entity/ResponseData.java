package com.supper.invincible_api.network.http.entity;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;

/**
 * 响应数据模型
 * @author admin
 *
 */
public class ResponseData {
	private String protocol; // 协议版本
	private String message; // 提示信息
	/** 返回状态码，目前200表示成功，其他只返回message字段，说明失败原因 **/
	private int code;
	// 扩展字段
	private Map<String, String> extra = new HashMap<String, String>();

	public String toJsonString() {
		return JSON.toJSONString(this);
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Map<String, String> getExtra() {
		return extra;
	}

	public void setExtra(Map<String, String> extra) {
		this.extra = extra;
	}

}
