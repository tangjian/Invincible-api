package com.supper.invincible_api.network.http.entity;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;

/**
 * 请求数据模型
 * 
 * @author admin
 */
public class RequestData {

	public static final String REQUESTDATA = RequestData.class.getSimpleName();
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;

	private String protocol; // 协议版本
	private String userName; // 用户Id
	private String userPassword; // 用户密码
	private String userId;// 用户id
	// 应用版本
	private String appVersion;
	// 手机型号
	private String mobileType;
	// 操作系统类型 andorid ,ios, windows phone
	private String mobileOSType;
	// 操作系统版本
	private String mobileOSVersionNo;

	private Map<String, String> extra = new HashMap<String, String>();// 扩展字段

	public RequestData() {
		// 初始化用户名密码以及userId;
	}

	public String toJsonString() {
		return JSON.toJSONString(this);
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Map<String, String> getExtra() {
		return extra;
	}

	public void setExtra(Map<String, String> extra) {
		this.extra = extra;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getMobileType() {
		return mobileType;
	}

	public void setMobileType(String mobileType) {
		this.mobileType = mobileType;
	}

	public String getMobileOSType() {
		return mobileOSType;
	}

	public void setMobileOSType(String mobileOSType) {
		this.mobileOSType = mobileOSType;
	}

	public String getMobileOSVersionNo() {
		return mobileOSVersionNo;
	}

	public void setMobileOSVersionNo(String mobileOSVersionNo) {
		this.mobileOSVersionNo = mobileOSVersionNo;
	}

}
