package com.supper.invincible_api.network.http;

import java.util.Map;

import com.android.volley.VolleyError;

public interface IHttpRequest {
	
    public void accessSucc(int requestCode, Map<String, String> data);

    public void accessFail(int requestCode, String errMsg);

    public void onVolleyError(VolleyError error);

    public void start();

}
