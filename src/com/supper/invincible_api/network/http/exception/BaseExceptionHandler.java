package com.supper.invincible_api.network.http.exception;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.supper.invincible_api.util.ToastManager;

public final class BaseExceptionHandler {

    public static void handleException(Context context, VolleyError error) {
        if (null != error) {
            if (error instanceof AuthFailureError) {
            	ToastManager.getInstance().showToast(context, "认证失败！");
            } else if (error instanceof NetworkError) {
                ToastManager.getInstance().showToast(context, "网络错误！");
            } else if (error instanceof NoConnectionError) {
                ToastManager.getInstance().showToast(context, "无网络连接！");
            } else if (error instanceof ServerError) {
                ToastManager.getInstance().showToast(context, "服务器异常！");
            } else if (error instanceof TimeoutError) {
                ToastManager.getInstance().showToast(context, "请求超时！");
            } else {
                ToastManager.getInstance().showToast(context, "未知错误！");
            }
        }
    }
}
