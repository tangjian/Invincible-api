package com.supper.invincible_api.adapter;

import com.android.volley.toolbox.ImageLoader;

import android.widget.BaseAdapter;

public abstract class BaseVolleyAdapter extends BaseAdapter {
	
	private ImageLoader mImageLoader = null;

	public ImageLoader getmImageLoader() {
		return mImageLoader;
	}

	public void setmImageLoader(ImageLoader mImageLoader) {
		this.mImageLoader = mImageLoader;
	}
	
}
