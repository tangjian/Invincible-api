package com.supper.invincible_api.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * @ClassName HOHODB
 * @Description TODO 数据库操作类
 * @author guoshuai
 * @date 2014年5月29日
 */
public class InvinDB {
    /**
     * @Method: query
     * @Description: TODO 查询方法， 传入要执行的sql 返回执行结果
     * @param @param context
     * @param @param sql
     * @param @return
     * @return List<Map<String,String>>
     * @throws
     */
    public static synchronized List<Map<String, String>> query(Context context, String sql) {
        ArrayList<Map<String, String>> tableData = new ArrayList<Map<String, String>>();
        if (context == null) {
            return tableData;
        }
        Map<String, String> columnData = null;
        InvincibleDBHelper helper = new InvincibleDBHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                columnData = new HashMap<String, String>();
                int columnCount = cursor.getColumnCount();
                for (int i = 0; i < columnCount; i++) {
                    String key = cursor.getColumnName(i);
                    String value = cursor.getString(i);
                    columnData.put(key, value);
                }
                tableData.add(columnData);
            }
        } finally {
            if (null != cursor)
                cursor.close();
            db.close();
        }
        return tableData;
    }

    /**
     * @Method: insert
     * @Description: TODO
     * @param @param context
     * @param @param tableName
     * @param @param values
     * @return void
     * @throws
     */
    public static void insert(Context context, String tableName, ContentValues values) {
        SQLiteDatabase db = getDatabase(context);
        try {
            db.insert(tableName, "", values);
        } finally {
            db.close();
        }
    }

    /**
     * @Method: update
     * @Description: TODO
     * @param @param context
     * @param @param sql
     * @return void
     * @throws
     */
    public static void update(Context context, String sql) {
        SQLiteDatabase db = getDatabase(context);
        try {
            db.execSQL(sql);
        } finally {
            db.close();
        }
    }

    /**
     * @Method: update
     * @Description: TODO
     * @param @param context
     * @param @param tableName
     * @param @param values
     * @param @param whereClause
     * @return void
     * @throws
     */
    public static void update(Context context, String tableName, ContentValues values, String whereClause) {
        SQLiteDatabase db = getDatabase(context);
        try {
            db.update(tableName, values, whereClause, null);
        } finally {
            db.close();
        }
    }

    /**
     * @Method: drop
     * @Description: TODO
     * @param @param context
     * @param @param tableName
     * @return void
     * @throws
     */
    public static void drop(Context context, String tableName) {
        SQLiteDatabase db = getDatabase(context);
        try {
            db.delete(tableName, null, null);
        } finally {
            db.close();
        }
    }

    /**
     * @Method: drop
     * @Description: TODO
     * @param @param context
     * @param @param tableName
     * @param @param whereClause
     * @return void
     * @throws
     */
    public static void drop(Context context, String tableName, String whereClause) {
        SQLiteDatabase db = getDatabase(context);
        try {
            db.delete(tableName, whereClause, null);
        } finally {
            db.close();
        }
    }

    /**
     * @Method: getDatabase
     * @Description: TODO 获得数据库对象。
     * @param @param context
     * @param @return
     * @return SQLiteDatabase
     * @throws
     */
    public static SQLiteDatabase getDatabase(Context context) {
        InvincibleDBHelper helper = new InvincibleDBHelper(context);
        return helper.getWritableDatabase();
    }
}
