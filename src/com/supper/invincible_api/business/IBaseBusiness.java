package com.supper.invincible_api.business;

import com.android.volley.VolleyError;

public interface IBaseBusiness {

    public void onBusinessSucc(int bCode, Object obj);

    public void onBusinessFail(int bCode, Object obj);
    
    public void setOnVolleyError(VolleyError error);

}
