package com.supper.invincible_api.business;

import com.android.volley.VolleyError;
import com.supper.invincible_api.network.http.HttpRequest;
import com.supper.invincible_api.network.http.IHttpRequest;

public abstract class BaseBusiness implements IHttpRequest{
	
	protected HttpRequest mHttpRequest = null;
	protected IBaseBusiness iBaseBusiness = null;
    protected int requestCode;
	
	public BaseBusiness() {
		this.mHttpRequest = new HttpRequest(this);
	}
	
	public BaseBusiness(IBaseBusiness baseBusiness) {
		this.iBaseBusiness = baseBusiness;
		this.mHttpRequest = new HttpRequest(this);
	}

	public BaseBusiness(IBaseBusiness baseBusiness, int requestCode) {
		this.iBaseBusiness = baseBusiness;
		this.mHttpRequest = new HttpRequest(this);
		this.mHttpRequest.setRequestCode(requestCode);
	}

    public abstract void start();
    
	@Override
	public void onVolleyError(VolleyError error) {
		iBaseBusiness.setOnVolleyError(error);
	}
}
