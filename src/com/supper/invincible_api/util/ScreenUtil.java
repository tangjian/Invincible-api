package com.supper.invincible_api.util;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * 功能描述：屏幕查看工具类
* <p>包名:com.hoho.mall.utils</p>
* <p>项目名:hohomall</p>
* <p>类名:ScreenUtil.java</p>
* <p>吼吼手机客户端 </p>
* <p>功能说明：TODO</p>
* @author liuzemin
* @create 2014年5月1日   下午5:20:45 发布
* @version 1.0
 */
public class ScreenUtil {
	/**
	 * 得到设备屏幕的宽度
	 */
	public static int getScreenWidth(Context context) {
		return context.getResources().getDisplayMetrics().widthPixels;
	}

	/**
	 * 得到设备屏幕的高度
	 */
	public static int getScreenHeight(Context context) {
		return context.getResources().getDisplayMetrics().heightPixels;
	}

	/**
	 * 把密度转换为像素
	 */
	public static int dip2px(Context context, float px) {
		final float scale = getScreenDensity(context);
		return (int) (px * scale + 0.5);
	}
	
	/**
	 * 得到设备的密度
	 */
	public static float getScreenDensity(Context context) {
		DisplayMetrics dm =context.getResources().getDisplayMetrics();  
        return dm.densityDpi;
	}
}
