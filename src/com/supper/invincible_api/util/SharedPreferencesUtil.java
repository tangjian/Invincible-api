package com.supper.invincible_api.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.supper.invincible_api.base.BaseApplication;
import com.supper.invincible_api.constant.Constants;

/**
 * 信息保存到文件工具
 * 
 * @author tangjian
 * 
 */
@SuppressLint("CommitPrefEdits")
public final class SharedPreferencesUtil {
	private static SharedPreferences mSharedPreferences;
	@SuppressWarnings("unused")
	private static Editor mEditor;
	private static SharedPreferences settingSharedPreferences;
	@SuppressWarnings("unused")
	private static Editor settingEditor;
	private static SharedPreferencesUtil instance = null;

	@SuppressLint("CommitPrefEdits")
	private SharedPreferencesUtil() {
		if (null == mSharedPreferences) {
			mSharedPreferences = BaseApplication.getmContext()
					.getSharedPreferences(Constants.HOHO_MALL_USER,
							Context.MODE_PRIVATE);
			mEditor = mSharedPreferences.edit();
		}

		if (null == settingSharedPreferences) {
			settingSharedPreferences = BaseApplication.getmContext()
					.getSharedPreferences(Constants.HOHO_MALL_SYSTEM,
							Context.MODE_PRIVATE);
			settingEditor = settingSharedPreferences.edit();
		}
	}

	public static SharedPreferencesUtil getInstance() {
		if (null == instance) {
			synchronized (SharedPreferencesUtil.class) {
				if (null == instance) {
					instance = new SharedPreferencesUtil();
				}
			}
		}
		return instance;
	}
}
