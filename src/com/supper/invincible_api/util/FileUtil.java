package com.supper.invincible_api.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;

public class FileUtil {
	public static Map<String, String> readPropertyFile(Context context,
			String fileName) throws IOException {
		Map<String, String> propertyMap = new HashMap<String, String>();
		InputStream in = null;
		in = context.getResources().getAssets().open(fileName);
		InputStreamReader inputStreamReader = new InputStreamReader(in, "utf-8");
		BufferedReader br = new BufferedReader(inputStreamReader);
		String temp = null;
		String[] sInfo = new String[2];
		temp = br.readLine();
		while (temp != null) {
			if (!temp.startsWith("#") && !temp.equalsIgnoreCase("")) {
				sInfo = temp.split("=");
				if (sInfo.length == 2)
					propertyMap.put(sInfo[0], sInfo[1]);
			}
			temp = br.readLine();
		}
		return propertyMap;
	}
}
