package com.supper.invincible_api.util;

import android.content.Context;
import android.util.Log;

public class INVINLog {
	private static final boolean debug = true;

	public static void log(String msg) {
		if (debug) {
			if (null != msg) {
				Log.d("HOHO", msg);
			}
		}
	}

	public static void log(String tag, String msg) {
		if (debug) {
			if (null != msg) {
				Log.d(tag, msg);
			}
		}
	}

	public static void err(String msg) {
		if (debug) {
			if (null != msg) {
				Log.e("HOHO", msg);
			}
		}
	}

	public static void showMsg(Context context, String msg) {
		if (debug) {
			Log.d("HOHO", msg);
			ToastManager.getInstance().showToast(context, msg);
		}
	}
}
