package com.supper.invincible_api.util;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

/**
 * @管理Toast显示
 * @author tangjian
 * 
 */
public class ToastManager {
	private static volatile ToastManager mInstance = null;
	private ToastInfo mPreShowToastInfo;
	/**
	 * 两个相同的Toast显示的时间间隔,以毫秒为单位.
	 */
	private final long mShowSameToastInterval = 2000;
	private Toast mToast;

	public ToastManager() {
	}

	public static ToastManager getInstance() {
		if (mInstance == null) {
			synchronized (ToastManager.class) {
				if (mInstance == null) {
					mInstance = new ToastManager();
				}
			}
		}
		return mInstance;
	}

	public void showToast(Context context, int resId) {
		if (context != null) {
			String text = context.getString(resId);
			showToast(context, text);
		}
	}

	public void showToastLong(Context context, int resId) {
		if (context != null) {
			String text = context.getString(resId);
			showToastLong(context, text);
		}
	}

	public void showToast(Context context, String text) {
		if (!TextUtils.isEmpty(text)) {
			showToast(context, text, Toast.LENGTH_SHORT);
		}
	}

	public void showToastLong(Context context, String text) {
		if (!TextUtils.isEmpty(text)) {
			showToast(context, text, Toast.LENGTH_LONG);
		}
	}

	public void showToast(Context context, String text, int duration) {
		if (mPreShowToastInfo == null) {
			updateToastInfoAndShowToast(context, text, duration);
		} else {
			if (!TextUtils.isEmpty(mPreShowToastInfo.text)
					&& mPreShowToastInfo.text.equals(text)) {
				/*
				 * 当接下来要显示的Toast的文本与先前的相同时,按照以下规则进行显示. #1>
				 * 如果两个Toast之间的时间间隔大于预定义的时间间隔,则显示第二个Toast. #2>
				 * 如果两个Toast的Context不相同时,则显示第二个Toast. #3>
				 * 如果不满足1,2则只更新时间.（PS：已改为不更新时间，效果为toast消失时再点击能正常提示）
				 */
				final Context preToastContext = mPreShowToastInfo.contextRef
						.get();
				if ((System.currentTimeMillis() - mPreShowToastInfo.showTime >= mShowSameToastInterval)
						|| (preToastContext != context)) {
					updateToastInfoAndShowToast(context, null, duration);
				}
				// else {
				// // 仅仅只更新保存的Toast的时间.
				// mPreShowToastInfo.showTime = System.currentTimeMillis();
				// }
			} else {
				updateToastInfoAndShowToast(context, text, duration);
			}
		}
	}

	/**
	 * 更新保存的Toast信息,然后显示Toast.
	 * 
	 * @param newContext
	 * @param newText
	 * @param duration
	 */
	private void updateToastInfoAndShowToast(Context newContext,
			String newText, int duration) {
		if (mPreShowToastInfo == null) {
			mPreShowToastInfo = new ToastInfo();
		}

		if (newText != null) {
			mPreShowToastInfo.text = newText;
		}

		if (newContext != null) {
			mPreShowToastInfo.contextRef = new WeakReference<Context>(
					newContext);
		}
		if (mToast == null) {
			mToast = Toast.makeText(newContext, mPreShowToastInfo.text,
					duration);
		} else {
			mToast.setDuration(duration);
			mToast.setText(mPreShowToastInfo.text);
		}
		mToast.show();
		mPreShowToastInfo.showTime = System.currentTimeMillis();
	}

	private static class ToastInfo {
		WeakReference<Context> contextRef;
		String text;
		long showTime;
	}
}
