package com.supper.invincible_api.base;

/**
 * @ClassName BaseContainer
 * @Description 
 *              <p>
 *              所有Activity、Fragment 都继承这个接口。
 *              </p>
 * @author guoshuai
 * @date 2014年7月17日
 * 
 */
public interface BaseContainer {
    /**
     * @Method: initView
     * @Description: 
     *               <p>
     *               初始化界面
     *               <p>
     * @param
     * @return void
     * @throws
     */
    public void initView();

    /**
     * @Method: setListener
     * @Description: 
     *               <p>
     *               初始化监听
     *               <p>
     * @param
     * @return void
     * @throws
     */
    public void setListener();

    /**
     * @Method: initData
     * @Description: 
     *               <p>
     *               初始化数据
     *               <p>
     * @param
     * @return void
     * @throws
     */
    public void initData();
}
