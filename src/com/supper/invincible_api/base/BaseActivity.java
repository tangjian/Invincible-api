package com.supper.invincible_api.base;

import android.os.Bundle;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.supper.invincible_api.R;
import com.supper.invincible_api.business.IBaseBusiness;
import com.supper.invincible_api.network.cache.BitmapCache;
import com.supper.invincible_api.util.ToastManager;
import com.supper.invincible_api.view.dialog.BaseProgressDialog;

public abstract class BaseActivity extends SlidingFragmentActivity implements
		BaseContainer, IBaseBusiness {

	protected BaseProgressDialog mBaseProgressDialog = null;
	private ImageLoader mImageLoader = null;
	private int mTitleRes;

	public BaseActivity(int mTitleRes) {
		this.mTitleRes = mTitleRes;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();
		initView();
		setListener();
		initData();
	}

	private void init() {
		mImageLoader = new ImageLoader(BaseApplication.getRequestQueue(),
				new BitmapCache());
	}

	/**
	 * @Method: showProgressDialog
	 * @Description: 显示等待对话框
	 * @param @param message
	 * @return void
	 * @throws
	 */
	public void showProgressDialog(String message) {
		mBaseProgressDialog = new BaseProgressDialog(this);
		mBaseProgressDialog.setMessage(message);
		mBaseProgressDialog.show();
	}

	/**
	 * @Method: showProgressDialog
	 * @Description: 显示等待对话框
	 * @param @param message
	 * @param @param isCancelable
	 * @return void
	 * @throws
	 */
	public void showProgressDialog(String message, boolean isCancelable) {
		mBaseProgressDialog = new BaseProgressDialog(this);
		mBaseProgressDialog.setMessage(message);
		mBaseProgressDialog.show();
	}

	/**
	 * @Method: closeProgressDialog
	 * @Description: 关闭对话框
	 * @param
	 * @return void
	 * @throws
	 */
	public void closeProgressDialog() {
		if (null != mBaseProgressDialog) {
			mBaseProgressDialog.dismiss();
			mBaseProgressDialog = null;
		}
	}

	/**
	 * @Method: showMessage
	 * @Description: 弹出吐司信息
	 * @param @param message
	 * @return void
	 * @throws
	 */
	public void showMessage(String message) {
		if (null != message) {
			ToastManager.getInstance().showToast(this, message);
		}
	}

	@Override
	protected void onDestroy() {
		setContentView(R.layout.none);
		if (mBaseProgressDialog != null) {
			mBaseProgressDialog.dismiss();
			mBaseProgressDialog = null;
		}
		super.onDestroy();
	}

	@Override
	public void initView() {
		/**
		 * 设置标题
		 */
		setTitle(mTitleRes);
	}

	@Override
	public void setListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBusinessSucc(int bCode, Object obj) {

	}

	@Override
	public void onBusinessFail(int bCode, Object obj) {

	}

	@Override
	public void setOnVolleyError(VolleyError error) {

	}

	public ImageLoader getmImageLoader() {
		return mImageLoader;
	}
//	@Override 
//	public void onStop() {
//	    mRequestQueue.cancelAll(this);
//	}
}
