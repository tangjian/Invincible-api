package com.supper.invincible_api.base;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.supper.invincible_api.util.INVINLog;

public class BaseApplication extends Application {
	
	private static BaseApplication mApplication;
    private static Context mContext; // 当前上下文
    protected String tag = System.currentTimeMillis() + "";
	private static RequestQueue mRequestQueue = null;
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		mContext = getApplicationContext();
		mRequestQueue = Volley.newRequestQueue(mContext);
	}
	
	public static Context getmContext() {
        return mContext;
    }

    public static BaseApplication getApplication() {
        return mApplication;
    }

    public String getTag() {
        return tag;
    }

    @Override
    public void onLowMemory() {
        /**
         * 低内存的时候主动释放所有线程和资源 PS:这里不一定每被都调用
         */
        // DefaultThreadPool.shutdown();
        // shutdownHttpClient();
        INVINLog.log("HOHOApplication  onLowMemory");
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        /**
         * 系统退出的时候主动释放所有线程和资源 PS:这里不一定被都调用
         */
        // DefaultThreadPool.shutdown();
        // shutdownHttpClient();
        INVINLog.log("HOHOApplication  onTerminate");
        super.onTerminate();
    }

	public static RequestQueue getRequestQueue() {
		return mRequestQueue;
	}

	public static void setRequestQueue(RequestQueue mRequestQueue) {
		BaseApplication.mRequestQueue = mRequestQueue;
	}

}
