package com.supper.invincible_api.base;

import android.os.Bundle;

import com.actionbarsherlock.app.SherlockFragment;
import com.android.volley.toolbox.ImageLoader;
import com.supper.invincible_api.business.IBaseBusiness;
import com.supper.invincible_api.network.cache.BitmapCache;

public abstract class BaseFragment extends SherlockFragment implements BaseContainer,
		IBaseBusiness {

	private ImageLoader mImageLoader = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mImageLoader = new ImageLoader(BaseApplication.getRequestQueue(),
				new BitmapCache());
	}

	public ImageLoader getmImageLoader() {
		return mImageLoader;
	}

	public void setmImageLoader(ImageLoader mImageLoader) {
		this.mImageLoader = mImageLoader;
	}
	
}
