package com.supper.invincible_api.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/***
 * 
 * @author Administrator
 * 
 */
public class InVinListView extends RecyclerView {

	public InVinListView(Context context) {
		super(context);
	}

	public InVinListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public InVinListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

}
