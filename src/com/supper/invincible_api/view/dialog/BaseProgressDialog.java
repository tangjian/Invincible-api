package com.supper.invincible_api.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;

import com.supper.invincible_api.R;;

public class BaseProgressDialog extends Dialog {

	public BaseProgressDialog(Context context) {
		super(context);
		this.setContentView(R.layout.custom_progressdialog);
		this.getWindow().getAttributes().gravity = Gravity.CENTER;
		this.setCancelable(false);
		this.setCanceledOnTouchOutside(false);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus) {
			ImageView imageView = (ImageView) this
					.findViewById(R.id.loadingImageView);
			AnimationDrawable animationDrawable = (AnimationDrawable) imageView
					.getBackground();
			animationDrawable.start();
		}

	}

	public void setMessage(String strMessage) {
		TextView tvMsg = (TextView) this.findViewById(R.id.id_tv_loadingmsg);
		if (tvMsg != null) {
			tvMsg.setText(strMessage);
		}
	}
}
